import numpy as np
import matplotlib.pyplot as plt
from modules.facet import facet as ft
from modules.node import node as nd

class OneDCompressibleSimulation:
    def __init__(self, x_start, x_end, nodes, x_init, rho_init, u_init, p_init, gamma, flux_limiter):
        # Creating positions of cell faces
        x_faces = np.linspace(x_start, x_end, nodes+1)

        # Initialising various things
        self.n_nodes = nodes
        self.nodes = np.empty(nodes, dtype=nd)
        self.facets = np.empty(nodes+1, dtype=ft)
        self.p = np.zeros(nodes)
        self.rho = np.zeros(nodes)
        self.u = np.zeros(nodes)
        self.e = np.zeros(nodes)
        self.current_time = 0

        # Determining the x positions of all of the nodes
        self.x = (x_faces[:-1] + x_faces[1:])/2

        # Storing the flux limiter function
        self.flux_limiter = flux_limiter

        # Defining what section of the initial conditions the code is currently in
        place = 0

        # Creating the first face (called right face because the loop that comes later sets the right face to the left face)
        # Note: facet and face will be used interchangeably
        right_face = ft(x_start, 1)
        self.facets[0] = right_face

        # Loop to initialise nodes and faces
        for i in range(nodes):
            left_face = right_face

            x = self.x[i]
            if x > x_init[place+1]:
                place += 1

            right_face = ft(x, 1)
            self.facets[i+1] = right_face
            new_node = nd(1, left_face, right_face, rho_init[place], u_init[place], p_init[place], gamma)
            self.nodes[i] = new_node

            left_face.set_right_node(new_node)
            right_face.set_left_node(new_node)

            if i > 0:
                left_face.calc_dx()
                left_node = self.nodes[i-1]
                new_node.set_left_node(left_node)
                left_node.set_right_node(new_node)
                """
                Note: parsing a node/face into a method of another node/face DOES NOT create another instance of the node/
                face. Hence, there will only be one instance of each node/face in the simulation and the nodes/faces store a
                reference to the face/node to the left and right of it. This can be seen by uncommenting the code bellow.
                It will show that the memory address of the left and right nodes/faces correspond with the previous nodes/faces.
                """
                # print("******* i = ", i, " ********")
                # print("Previous node: ", left_node)
                # try:
                #     print("Left of previous node: ", left_node.left_node)
                # except():
                #     None
                # print("Right of previous node: ", left_node.right_node)
                # print("Left face of previous node: ", left_node.left_face)
                # print("Right face of previous node: ", left_node.right_face)



    def solve(self, t, dt):
        # Solving procedure, fairly self explanatory
        self.current_t = 0
        while self.current_t < t:
            for i in range(2, self.n_nodes-1):
                self.facets[i].calc_F_bar(self.flux_limiter)
            for i in range(2, self.n_nodes-2):
                self.nodes[i].step(dt)
            self.current_t += dt


    def extract_p(self):
        # Storing pressure values from nodes in a local array
        for i in range(self.n_nodes):
            self.p[i] = self.nodes[i].get_pressure()

    def extract_rho(self):
        # Storing density values from nodes in a local array
        for i in range(self.n_nodes):
            self.rho[i] = self.nodes[i].get_density()

    def extract_u(self):
        # Storing velocity values from nodes in a local array
        for i in range(self.n_nodes):
            self.u[i] = self.nodes[i].get_velocity()

    def extract_e(self):
        # Storing internal energy values from nodes in a local array
        for i in range(self.n_nodes):
            self.e[i] = self.nodes[i].get_e()

    def extract_all(self):
        # One function to extract all values of interest
        self.extract_p()
        self.extract_e()
        self.extract_u()
        self.extract_rho()

    #############################################
    ######### Some plotting methods #############
    #############################################

    def plot_pressure(self):
        self.extract_p()
        plt.plot(self.x, self.p, marker='^')
        plt.xlabel("x")
        plt.ylabel("p")
        plt.title("Pressure plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_e(self):
        self.extract_e()
        plt.plot(self.x, self.e, marker='^')
        plt.xlabel("x")
        plt.ylabel("e")
        plt.title("Internal energy plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_rho(self):
        self.extract_rho()
        plt.plot(self.x, self.rho, marker='^')
        plt.xlabel("x")
        plt.ylabel("$\\rho$")
        plt.title("Density plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_u(self):
        self.extract_u()
        plt.plot(self.x, self.u, marker='^')
        plt.xlabel("x")
        plt.ylabel("u")
        plt.title("Velocity plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_all(self):
        self.plot_pressure()
        self.plot_e()
        self.plot_rho()
        self.plot_u()
