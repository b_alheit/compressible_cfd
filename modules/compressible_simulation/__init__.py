import numpy as np
import matplotlib.pyplot as plt
from modules.facet import facet as ft
from modules.node import node as nd
from modules import time_stepping_schemes as ts
from modules.analytical_shock_tube import AnalyticalShockTube2


class OneDCompressibleSimulation:
    def __init__(self, x_start, x_end, nodes, x_init, rho_init, u_init, p_init, gamma, flux_limiter):
        # Creating positions of cell faces
        x_faces = np.linspace(x_start, x_end, nodes+1)

        # Initialising various things
        self.n_nodes = nodes
        self.nodes = np.empty(nodes, dtype=nd)
        self.facets = np.empty(nodes+1, dtype=ft)
        self.p = np.zeros(nodes)
        self.rho = np.zeros(nodes)
        self.u = np.zeros(nodes)
        self.e = np.zeros(nodes)
        self.current_time = 0
        self.state = np.zeros([3, nodes])
        self.state_n1 = np.zeros([3, nodes])
        self.gamma = gamma
        self.rho_init = rho_init
        self.u_init = u_init
        self.p_init = p_init

        # Determining the x positions of all of the nodes
        self.x = (x_faces[:-1] + x_faces[1:])/2
        self.dx = self.x[1] - self.x[0]

        # Storing the flux limiter function
        self.flux_limiter = flux_limiter

        # Defining what section of the initial conditions the code is currently in
        place = 0

        # Creating the first face (called right face because the loop that comes later sets the right face to the left face)
        # Note: facet and face will be used interchangeably
        right_face = ft(x_start, 1)
        self.facets[0] = right_face

        # Loop to initialise nodes and faces
        for i in range(nodes):
            left_face = right_face

            x = self.x[i]
            if x > x_init[place+1]:
                place += 1

            right_face = ft(x, 1)
            self.facets[i+1] = right_face
            new_node = nd(1, left_face, right_face, rho_init[place], u_init[place], p_init[place], gamma)
            self.nodes[i] = new_node

            left_face.set_right_node(new_node)
            right_face.set_left_node(new_node)

            if i > 0:
                left_face.calc_dx()
                left_node = self.nodes[i-1]
                new_node.set_left_node(left_node)
                left_node.set_right_node(new_node)
                """
                Note: parsing a node/face into a method of another node/face DOES NOT create another instance of the node/
                face. Hence, there will only be one instance of each node/face in the simulation and the nodes/faces store a
                reference to the face/node to the left and right of it. This can be seen by uncommenting the code bellow.
                It will show that the memory address of the left and right nodes/faces correspond with the previous nodes/faces.
                """
                # print("******* i = ", i, " ********")
                # print("Previous node: ", left_node)
                # try:
                #     print("Left of previous node: ", left_node.left_node)
                # except():
                #     None
                # print("Right of previous node: ", left_node.right_node)
                # print("Left face of previous node: ", left_node.left_face)
                # print("Right face of previous node: ", left_node.right_face)



    # def solve(self, t, dt):
    #     # Solving procedure, fairly self explanatory
    #     self.current_t = 0
    #     while self.current_t < t:
    #         for i in range(2, self.n_nodes-1):
    #             self.facets[i].calc_F_bar(self.flux_limiter)
    #         for i in range(2, self.n_nodes-2):
    #             self.nodes[i].step(dt)
    #         self.current_t += dt

    def solve(self, t, dt, time_integrator):
        self.current_t = 0
        self.extract_state()
        while self.current_t < t:
            self.state = time_integrator(self.state, self.g, dt)
            self.current_t += dt
        self.set_state(self.state)


    def solve_SOBD(self, t, dt):
        self.current_t = 0
        self.extract_state()
        self.state_n1 = self.state.copy()

        analytical = AnalyticalShockTube2(self.rho_init[0], self.u_init[0][0], self.p_init[0],
                                          self.rho_init[1], self.u_init[1][0], self.p_init[1],
                                          self.gamma, 0, dt, self.x)

        self.state[0, :] = analytical.rho
        self.state[1, :] = analytical.rho * analytical.u
        self.state[2, :] = analytical.rho * (analytical.e + 0.5 * analytical.u * analytical.u)
        self.current_t += dt

        # ui = self.state[1, :] / self.state[0, :]
        # Ei = self.state[2, :] / self.state[0, :]
        # Hi = self.gamma * Ei + (1 - self.gamma) * ui * ui / 2
        # ai = ((self.gamma - 1) * (Hi - ui * ui / 2)) ** 0.5
        # dti = 0.9 * self.dx / (np.abs(ui) + ai)
        # self.state = self.state = ts.RK4(self.state, self.g, np.min(dti))
        # self.current_t += np.min(dti)

        while self.current_t < t:
            ui = self.state[1, :] / self.state[0, :]
            Ei = self.state[2, :] / self.state[0, :]
            Hi = self.gamma * Ei + (1 - self.gamma) * ui *ui/2
            ai = ((self.gamma-1) * (Hi - ui * ui/2)) ** 0.5
            dti = 1 * self.dx / (np.abs(ui) + ai)
            dtaui = dti / (1 + (3/2)*(dti/dt))
            place_holder = self.state.copy()
            self.state = ts.SOBD(self.state, self.g, dt, self.state_n1, dtaui)
            self.state_n1 = place_holder
            self.current_t += dt
            # print(self.current_t)
        self.set_state(self.state)


    def solve_FE(self, t, dt):
        self.current_t = 0
        self.extract_state()
        while self.current_t < t:
            self.state = ts.FE(self.state, self.g, dt)
            self.current_t += dt
        self.set_state(self.state)

    def solve_RK4(self, t, dt):
        self.current_t = 0
        self.extract_state()
        while self.current_t < t:
            self.state = ts.RK4(self.state, self.g, dt)
            self.current_t += dt
        self.set_state(self.state)


    def extract_p(self):
        # Storing pressure values from nodes in a local array
        for i in range(self.n_nodes):
            self.p[i] = self.nodes[i].get_pressure()

    def extract_rho(self):
        # Storing density values from nodes in a local array
        for i in range(self.n_nodes):
            self.rho[i] = self.nodes[i].get_density()

    def extract_u(self):
        # Storing velocity values from nodes in a local array
        for i in range(self.n_nodes):
            self.u[i] = self.nodes[i].get_velocity()

    def extract_e(self):
        # Storing internal energy values from nodes in a local array
        for i in range(self.n_nodes):
            self.e[i] = self.nodes[i].get_e()

    def extract_all(self):
        # One function to extract all values of interest
        self.extract_p()
        self.extract_e()
        self.extract_u()
        self.extract_rho()

    def extract_state(self):
        for i in range(self.n_nodes):
            self.state[:, i] = self.nodes[i].U.flatten()

    def set_state(self, state):
        for i in range(self.n_nodes):
            self.nodes[i].U = np.array([state[:, i]]).T


    def g(self, state):
        if len(state.shape) == 1:
            state.reshape([3, int(np.alen(state)/3)])
        q = np.zeros(state.shape)
        self.set_state(state)

        for i in range(2, self.n_nodes - 1):
            self.facets[i].calc_F_bar(self.flux_limiter)

        for i in range(2, self.n_nodes - 2):
            q[:, i] = self.nodes[i].g().flatten()
        return q



    #############################################
    ######### Some plotting methods #############
    #############################################

    def plot_pressure(self):
        self.extract_p()
        plt.plot(self.x, self.p, marker='^')
        plt.xlabel("x")
        plt.ylabel("p")
        plt.title("Pressure plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_e(self):
        self.extract_e()
        plt.plot(self.x, self.e, marker='^')
        plt.xlabel("x")
        plt.ylabel("e")
        plt.title("Internal energy plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_rho(self):
        self.extract_rho()
        plt.plot(self.x, self.rho, marker='^')
        plt.xlabel("x")
        plt.ylabel("$\\rho$")
        plt.title("Density plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_u(self):
        self.extract_u()
        plt.plot(self.x, self.u, marker='^')
        plt.xlabel("x")
        plt.ylabel("u")
        plt.title("Velocity plot at t = " + str(self.current_t)[:4])
        plt.grid()
        plt.show()

    def plot_all(self):
        self.plot_pressure()
        self.plot_e()
        self.plot_rho()
        self.plot_u()
