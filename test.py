from modules.compressible_simulation import OneDCompressibleSimulation
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]


# # test 1
# test_cell = node(1, None, None, ft(), ft())
#
# print(test_cell.left_face.H_L)
#
# test_cell.left_face.H_L = 5
# print(test_cell.F)
# print(test_cell.u)
# print(test_cell.U)
#
# ref_cell = test_cell
#
# print(ref_cell.left_face.p_L, " ", test_cell.left_face.p_L)
#
# ref_cell.left_face.p_L = 10
# print(ref_cell)
# print(test_cell)

# # test 2
# dim = 2
# u = 3 * np.ones([dim, 1])
# print(np.concatenate((np.concatenate((np.zeros([1, dim]), np.identity(dim)), axis=0), u.T), axis=0))

# test 3
#
# def van_albada_fl(phi, phi_L):
#     return np.divide(np.power(phi, 2) + np.multiply(phi, phi_L), 1e-20 + np.power(phi, 2) + np.power(phi_L, 2))
#
# def van_leer_fl(phi, phi_L):
#     return np.divide(phi + np.multiply(np.sign(phi_L), np.abs(phi)), 1e-20 + phi_L + np.multiply(np.sign(phi_L), np.abs(phi)))
#
# x_init = np.array([-0.5, 0, 0.51])
# rho_init = np.array([1, 0.125])
# u_init = np.array([np.array([[0]]), np.array([[0]])])   # Case 1
# # u_init = np.array([np.array([[0.25]]), np.array([[0]])])    # Case 2
# p_init = np.array([1.0, 0.1])
# gamma = 1.4
#
# sim = OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init, p_init, gamma, van_albada_fl)
# sim.solve(0.2, 0.002)
# sim.extract_all()

a = np.array([[1, 2, 3],
                [4, 5, 6]]
               )

print(a)
# print()
print((0 <= a) & (a<= 2))
# print(0 <= a <= 2)
print(a.flatten())
print(a.reshape([2,3]))
# print(0 <= a <= 2)
for i in range(0, 10, int(10/5)):
    print(i)
