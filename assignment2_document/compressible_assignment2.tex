\documentclass{article}

\author{Benjamin Alheit}
\date{\today}
\title{MEC5069Z Temporal Integration Assignment}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage[margin = 0.8in]{geometry}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{bm}
\usepackage{float}
\usepackage{subfig}
\usepackage[backend=biber]{biblatex}
\newcommand{\specialcell}[2][c]{\begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}
\bibliography{ref.bib}
%\usepackage[nottoc]{tocbibind}


\begin{document}
\maketitle
\section{Introduction}

The objective of this assignment was to implement various time integration schemes and evaluate their performance. The time integration schemes in question were the forward Euler method, Heun's method, Runge-Kutta 4 and the second order backward difference method. The model problem used to evaluate the time schemes was the Sod shock tube with the initial conditions as shown in table \ref{tab:init} where $\rho$ is density, $u$ is velocity and $p$ is pressure.
\begin{table}[h!]
\centering
\caption[Caption for LOF]{Initial conditions used in problem. \footnotemark}
\begin{tabular}{c | c c c | c c c} 
 \hline
case& $\rho_L$ & $u_L$ & $p_L$ & $\rho_R$ & $u_R$ & $p_R$    \\ 
 \hline\hline
 1 & 1.0& 0.0 & 1.0& 0.125& 0.0 & 0.1\\ 
 \hline
\end{tabular}
\label{tab:init}
\end{table} 
\footnotetext{Subscripts $L$ and $R$ denote the initial left and right rates respectively and $\rho$, $u$ and $p$ are density, velocity and pressure respectively.}
The problem was run to a time of $0.2$ and time steps of $5\times10^{-3}$, $2.5\times10^{-3}$ and $1.25\times10^{-3}$ were used. 

\section{Results and discussion}
The results of the simulations for time steps of $5\times10^{-3}$, $2.5\times10^{-3}$ and $1.25\times10^{-3}$ are shown in figures \ref{fig:05}, \ref{fig:025} and \ref{fig:0125} respectively. In this case, the forward Euler method is unstable for a time step size of $5\times10^{-3}$, hence the results produced by the forward Euler scheme were excluded for this time step size. It is also apparent that at a time step of $5\times10^{-3}$ Heun's method displays severe spurious oscillations, which is typical of higher-order time integration schemes, particularly when they are used in the context of Computational Fluid Dynamics (CFD) \cite{spur}. \\
\\
It is interesting to note that, although higher-order time integration schemes are generally perceived as superior to low-order schemes, the low-order forward Euler scheme actually out performs the higher-order schemes in terms of capturing the shock front. This is likely due to that fact that all higher-order schemes used in this assignment use a weighted average of the state at consecutive times. This leads to the `smearing' of the shock front over a multitude of cells as the shock front moves through consecutive cells over the averaged consecutive times. However, the forward Euler method comes at the cost of requiring small time steps to maintain stability. Additionally, the higher-order time integration schemes smooth out the overshoot in the internal energy distribution caused by the flux limiter, whereas the forward Euler method does not.\\
\\
Apart from the spurious oscillations produced by Heun's method, there is little difference between the results produced by the higher-order methods. The only other possible point of comparison is the simulation runtime for each method. These times can be seen in table \ref{tab:times}.
\begin{table}[h!]
\centering
\caption{Simulation runtimes in seconds}
\begin{tabular}{c | c c c c} 
 \hline
Time step size & Forward Euler & Heun's method & Runge-Kutta 4 & \specialcell{Second order \\ backward difference } \\ 
 \hline\hline
 $5\times10^{-3}$ & - & 1.24 & 2.48 & 12.5\\ 
 $2.5\times10^{-3}$ & 1.19 & 2.35 & 4.72 & 15.2 \\ 
 $1.25\times10^{-3}$ & 3.54 & 5.79 & 11.7 & 27.1\\ 
 \hline
\end{tabular}
\label{tab:times}
\end{table} 
The typical trend in the runtime results is that Runge-Kutta 4 takes approximately twice as long to run as Heun's method which takes approximately twice as long as the forward Euler method for the same time step size. This is unsurprising since Runge-Kutta 4 requires twice as many derivative terms as Heun's method which requires twice as many derivative terms as the forward Euler method for each time step. This may make it seem like the forward Euler scheme is preferable over the other two, however Runge-Kutta 4 is stable for much larger time steps than the forward Euler method. The forward Euler method seems to be on the edge of stability for a time step of $2.5\times10^{-3}$ due to the presence of small perturbations near the shock front in internal energy and velocity. Hence, a time step of approximately $1.25\times10^{-3}$ would be required for the forward Euler method to be `comfortably' stable, yielding a runtime of $3.54\; s$, whereas Runge-Kutta 4 is stable at a time step size of $5\times10^{-3}$ (and possibly larger), yielding a runtime of $2.48 \; s$. Similarly, Heun's method produces acceptable results at a time step size of $2.5\times10^{-3}$, yielding a runtime of $2.35 \; s$.\\
\\
The second order backward difference method takes a significantly larger amount of runtime than the other methods used. This is due to the fact that it requires an implicit solve for the state at the next time step. The benefit of this, however, is that the method is L-stable, hence it will be stable for any time step size. It is also interesting to note that, whereas the runtimes for forward Euler, Heun's method and  Runge-Kutta 4 all scale directly proportionally with the time step used, the second order backward difference method does not. This is likely due to the fact that the amount of iterations required for the implicit solve is not directly proportional to the time step size.


\begin{figure}[H]
\centering
\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth]{p05}
\caption{Pressure}
\label{fig:p05}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth]{e05}
\caption{Internal energy}
\label{fig:e05}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth]{rho05}
\caption{Density}
\label{fig:rho05}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.9\linewidth]{u05}
\caption{Velocity}
\label{fig:u05}
\end{subfigure}

\caption{Time step of $5\times 10^{-3}$}
\label{fig:05}
\end{figure}


\begin{figure}[H]
\centering
\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{p025}
\caption{Pressure}
\label{fig:p025}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{e025}
\caption{Internal energy}
\label{fig:e025}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{rho025}
\caption{Density}
\label{fig:rho025}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{u025}
\caption{Velocity}
\label{fig:u025}
\end{subfigure}

\caption{Time step of $2.5\times 10^{-3}$}
\label{fig:025}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{p0125}
\caption{Pressure}
\label{fig:p0125}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{e0125}
\caption{Internal energy}
\label{fig:e0125}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{rho0125}
\caption{Density}
\label{fig:rho0125}
\end{subfigure}

\begin{subfigure}{\textwidth}
\includegraphics[width=0.96\linewidth]{u0125}
\caption{Velocity}
\label{fig:u0125}
\end{subfigure}

\caption{Time step of $1.25\times 10^{-3}$}
\label{fig:0125}
\end{figure}


\printbibliography

\end{document}
