from modules.compressible_simulation import OneDCompressibleSimulation
import numpy as np
from modules.analytical_shock_tube import AnalyticalShockTube
import matplotlib
import matplotlib.pyplot as plt
# matplotlib.rc('text', usetex=True)
# matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
# markers = ['^', 'X', 's', 'v', 'o', 'x', 'P']
markers = [None, None, None, None, None, None]
styles = ['--', '-.', ':']


def van_albada_fl(phi, phi_L):
    return np.divide(np.power(phi, 2) + np.multiply(phi, phi_L), 1e-20 + np.power(phi, 2) + np.power(phi_L, 2))

def van_leer_fl(phi, phi_L):
    return np.divide(phi + np.multiply(np.sign(phi_L), np.abs(phi)), 1e-20 + phi_L + np.multiply(np.sign(phi_L), np.abs(phi)))

def first_order_fl(phi, phi_L):
    return 0*phi

x_init = np.array([-0.5, 0, 0.5])
rho_init = np.array([1, 0.125])
u_init1 = np.array([np.array([[0]]), np.array([[0]])])   # Case 1
p_init = np.array([1.0, 0.1])
gamma = 1.4

case1_analytical = AnalyticalShockTube(0.2)

simulations = np.empty(0, dtype=OneDCompressibleSimulation)
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, first_order_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_albada_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_leer_fl)])

labels = ["Case 1 first order", "Case 1 Van Albada", "Case 1 Van Leer", "Case 2 Van Albada", "Case 2 Van Leer", "Case 2 first order"]

for i in range(np.alen(simulations)):
    simulations[i].solve(0.2, 0.002)
    simulations[i].extract_all()

x = simulations[0].x.tolist()

fig, ax = plt.subplots(4, 1)
# ax.sharex()
ax[3].xlabel("x")
# plt.subplot(4, 1, 1)
ax[0].plot(case1_analytical.x, case1_analytical.P, color='black', linewidth=3, linestyle="-", marker=None, label="Case 1 Analytical")
# plt.subplot(4, 1, 2)
ax[1].plot(case1_analytical.x, case1_analytical.e, color='black', linewidth=3, linestyle="-", marker=None, label="Case 1 Analytical")
# plt.subplot(4, 2, 1)
ax[2].plot(case1_analytical.x, case1_analytical.rho, color='black', linewidth=3, linestyle="-", marker=None, label="Case 1 Analytical")
# plt.subplot(4, 2, 2)
ax[3].plot(case1_analytical.x, case1_analytical.u, color='black', linewidth=3, linestyle="-", marker=None, label="Case 1 Analytical")

for i in range(np.alen(simulations)):
    hexColour = str(hex(int((i+1) * 150 / (np.alen(simulations)))))[2:]
    if len(hexColour) == 1:
        hexColour = '0' + hexColour

    # plt.subplot(4, 1, 1)
    ax[0].plot(x, simulations[i].p.tolist(), color='#' + 3*hexColour, linewidth=2, linestyle=styles[i], marker=markers[i], label=labels[i])
    # plt.subplot(4, 1, 2)
    ax[1].plot(x, simulations[i].e.tolist(), color='#' + 3*hexColour, linewidth=2, linestyle=styles[i], marker=markers[i], label=labels[i])
    # plt.subplot(4, 2, 1)
    ax[2].plot(x, simulations[i].rho.tolist(), color='#' + 3*hexColour, linewidth=2, linestyle=styles[i], marker=markers[i], label=labels[i])
    # plt.subplot(4, 2, 2)
    ax[3].plot(x, simulations[i].u.tolist(), color='#' + 3*hexColour, linewidth=2, linestyle=styles[i], marker=markers[i], label=labels[i])

# plt.subplot(4, 1, 1)
# ax[0].xlim(-0.5, 0.5)
# ax[0].grid()
# ax[0].xlabel("$x$", fontsize=12)
# ax[0].ylabel("$p$", fontsize=12)
# # plt.title("Pressure distributions for various simulations", fontsize=12)
# ax[0].legend()
#
# # plt.subplot(4, 1, 2)
# # ax[1].xlim(-0.5, 0.5)
# ax[1].grid()
# ax[1].xlabel("$x$", fontsize=12)
# ax[1].ylabel("$e$", fontsize=12)
# # plt.title("Internal energy distributions for various simulations", fontsize=12)
# ax[1].legend()
#
# # plt.subplot(4, 2, 1)
# # ax[2].xlim(-0.5, 0.5)
# ax[2].grid()
# ax[2].xlabel("$x$", fontsize=12)
# ax[2].ylabel("$\\rho$", fontsize=12)
# # plt.title("Density distributions for various simulations", fontsize=12)
# ax[2].legend()
#
# # ax[3].subplot(4, 2, 2)
# # ax[3].xlim(-0.5, 0.5)
# ax[3].grid()
# ax[3].xlabel("$x$", fontsize=12)
# ax[3].ylabel("$u$", fontsize=12)
# # plt.title("Velocity distributions for various simulations", fontsize=12)
# ax[3].legend()

plt.show()

