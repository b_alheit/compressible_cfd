import numpy as np

class node:
    def __init__(self, dim, left_face, right_face, rho, u, p, gamma):
        """
        Constructor for a node in a compressible cfd simulation. The code was written for 1D implementation
        vec(u) = [u  v  w]^T
        U = \rho[1  vec(u)  E]^T
        F = U vec(u)^T + p[vec(0)  I  vec(u)^T]^T
        """
        # Assigning the dimension of the node
        self.dim = dim

        # Initialising some values
        self.gamma = gamma
        e = p / (rho * (gamma-1))
        E = e + 0.5 * np.dot(u, u)
        self.u = u
        self.U = rho * np.concatenate((np.concatenate((np.array([[1]]), u), axis=0), E), axis=0)
        self.F = np.matmul(self.U, self.u.T) + p * np.concatenate((np.concatenate((np.zeros([1, dim]), np.identity(dim)), axis=0), self.u.T), axis=0)
        self.x = np.zeros([dim, 1])
        self.V = right_face.x - left_face.x
        self.x[0, 0] = 0.5 * (left_face.x + right_face.x)
        self.rho = 0
        self.E = 0
        self.H = 0
        self.P = 0

        # Assigning the left and the right faces of the node
        self.left_face = left_face
        self.right_face = right_face

        # Creating variables to store the left and right nodes of the node
        self.left_node = None
        self.right_node = None


    def set_left_node(self, in_node):
        # Defining the left node
        self.left_node = in_node

    def set_right_node(self, in_node):
        # Defining the right node
        self.right_node = in_node

    def step(self, dt):
        # Applying a forward Euler time step to the node
        self.F = (-self.left_face.F_bar + self.right_face.F_bar) / self.V
        self.U -= dt * self.F

    def get_pressure(self):
        # Returning the value of the pressure at the node
        e = self.U[-1] / self.U[0] - 0.5 * np.dot(self.U[1:-1], self.U[1:-1]) / self.U[0]**2
        return (self.gamma - 1) * self.U[0] * e

    def get_density(self):
        # Returning the value of the density at the node
        return self.U[0]

    def get_velocity(self):
        # Returning the value of the velocity at the node
        return self.U[1:-1] / self.U[0]

    def get_e(self):
        # Returning the value of the internal energy at the node
        return self.U[-1] / self.U[0] - 0.5 * np.dot(self.U[1:-1], self.U[1:-1]) / self.U[0]**2

