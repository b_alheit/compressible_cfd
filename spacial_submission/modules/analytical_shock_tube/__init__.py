import math
import scipy.optimize as sp
import numpy as np

class AnalyticalShockTube2:
    def __init__(self, rhoL, uL, PL, rhoR, uR, PR, gamma, x0, t, x):
        # SOD_SHOCK_TUBE computes the analytical solution to the Riemann problem of
        #    Euler equations. This solution is only valid for a
        #    rarefaction-shock combination (see
        #    http://www.scielo.org.mx/pdf/rmfe/v59n1/v59n1a5.pdf)
        #
        #    Inputs:
        #        rhoL  - Left density
        #        uL    - Left velocity
        #        PL    - Left pressure
        #        rhoR  - Right density
        #        uR    - Right velocity
        #        PR    - Right pressure
        #        gamma - Ratio of specific heat capacities
        #        x0    - Start position of the discontinuity
        #        t     - Time at which solution is desired
        #        x     - Positions at which solution is desired
        #
        #    Outputs:
        #        self.x   - Position
        #        self.rho - Density
        #        self.u   - Velocity
        #        self.P   - Pressure
        #        self.e   - Internal energy

        self.x = x
        self.rho = np.zeros(np.alen(x))
        self.u = np.zeros(np.alen(x))
        self.P = np.zeros(np.alen(x))
        self.e = np.zeros(np.alen(x))

        ## Computing states

        # Region 1 - Left
        aL = math.sqrt(gamma * PL / rhoL)

        # Region 5 - Right
        aR = math.sqrt(gamma * PR / rhoR)
        AR = 2 / ((gamma + 1) * rhoR)
        BR = PR * (gamma - 1) / (gamma + 1)

        # Region 2 - Rarefaction Fan
        rho2 = lambda x: rhoL * (2 / (gamma + 1) + (gamma - 1) / (aL * (gamma + 1)) * (uL - (x - x0) / t)) ** (
                    2 / (gamma - 1))
        u2 = lambda x: 2 / (gamma + 1) * (aL + (gamma - 1) * uL / 2 + (x - x0) / t)
        P2 = lambda x: PL * (2 / (gamma + 1) + (gamma - 1) / (aL * (gamma + 1)) * (uL - (x - x0) / t)) ** (
                    2 * gamma / (gamma - 1))

        # Region 3 & 4 - Contact Wave
        PFunc = lambda p: (p - PR) * math.sqrt(AR / (p + BR)) + 2 * aL / (gamma - 1) * (
                    (p / PL) ** ((gamma - 1) / (2 * gamma)) - 1) + (uR - uL)
        PS = sp.newton(PFunc, (PL + PR) / 2)

        # Region 3 - After rarefaction
        P3 = PS
        rho3 = rhoL * (P3 / PL) ** (1 / gamma)
        u3 = uL - 2 * aL / (gamma - 1) * ((P3 / PL) ** ((gamma - 1) / (2 * gamma)) - 1)
        a3 = math.sqrt(gamma * P3 / rho3)

        # Region 4 - Before shock
        P4 = PS
        rho4 = rhoR * (PR * (gamma - 1) + P4 * (gamma + 1)) / (P4 * (gamma - 1) + PR * (gamma + 1))
        u4 = uR + (P4 - PR) * math.sqrt(AR / (P4 + BR))

        # Speeds
        vHead = uL - aL
        vTail = u3 - a3
        vCont = u3
        vShoc = uR + aR * math.sqrt((gamma + 1) * P4 / (2 * gamma * PR) + (gamma - 1) / (2 * gamma))

        ## Calculating solution
        reg1 = (self.x - x0) <= t * vHead
        reg2 = (t * vHead <= (self.x - x0)) & ((self.x - x0) <= t * vTail)
        reg3 = (t * vTail <= (self.x - x0)) & ((self.x - x0) <= t * vCont)
        reg4 = (t * vCont <= (self.x - x0)) & ((self.x - x0) <= t * vShoc)
        reg5 = t * vShoc <= (self.x - x0)

        self.rho[reg1] = rhoL
        self.u[reg1] = uL
        self.P[reg1] = PL

        self.rho[reg2] = rho2(self.x[reg2])
        self.u[reg2] = u2(self.x[reg2])
        self.P[reg2] = P2(self.x[reg2])

        self.rho[reg3] = rho3
        self.u[reg3] = u3
        self.P[reg3] = P3

        self.rho[reg4] = rho4
        self.u[reg4] = u4
        self.P[reg4] = P4

        self.rho[reg5] = rhoR
        self.u[reg5] = uR
        self.P[reg5] = PR

        self.e = self.P / ((gamma - 1) * self.rho)


def sod_func(P):
    rho_l = 1
    P_l = 1
    u_l = 0

    rho_r = 0.125
    P_r = 0.1
    u_r = 0

    gamma = 1.4

    mu = math.sqrt((gamma - 1) / (gamma + 1))

    return (P - P_r) * ((1 - mu * mu) * (rho_r * (P + mu * mu * P_r)) ** -1) ** (0.5) - (P_l ** ((gamma - 1) / (2 * gamma)) - P ** ((gamma - 1) / (2 * gamma))) * (((1 - mu * mu * mu * mu) * P_l ** (1 / gamma)) * (mu * mu * mu * mu * rho_l) ** -1) ** 0.5

class AnalyticalShockTube:
    # #to solve Sod's Shock Tube problem
    # #reference: "http://www.phys.lsu.edu/~tohline/PHYS7412/sod.html"
    # #   |       |   |     |         |
    # #   |       |   |     |         |
    # #   |       |   |     |         |
    # #___|_______|___|_____|_________|_______________
    # #   x1      x2  x0    x3        x4
    # #
    def __init__(self, t):
        # #input require: t (time)
        # #Initial conditions
        x0 = 0
        rho_l = 1
        P_l = 1
        u_l = 0

        rho_r = 0.125
        P_r = 0.1
        u_r = 0

        gamma = 1.4
        mu = math.sqrt((gamma-1)/(gamma+1) )

        # #speed of sound
        c_l = (gamma*P_l/rho_l)**(0.5)
        c_r = (gamma*P_r/rho_r)**(0.5)

        P_post = sp.newton(sod_func, (P_l + P_r)/2)
        v_post = 2*(math.sqrt(gamma)/(gamma - 1))*(1 - P_post ** ((gamma - 1)/(2*gamma)))
        rho_post = rho_r*(( (P_post/P_r) + mu**2 )/(1 + mu*mu*(P_post/P_r)))
        v_shock = v_post*((rho_post/rho_r)/( (rho_post/rho_r) - 1))
        rho_middle = (rho_l)*(P_post/P_l) ** (1/gamma)

        # #Key Positions
        x1 = x0 - c_l*t
        x3 = x0 + v_post*t
        x4 = x0 + v_shock*t
        # #determining x2
        c_2 = c_l - ((gamma - 1)/2)*v_post
        x2 = x0 + (v_post - c_2)*t

        # #start setting values
        n_points = 1000    # #set by user
        # #boundaries (can be set)
        x_min = -0.5
        x_max = 0.5

        x = np.linspace(x_min,x_max,n_points)
        self.x = x
        self.rho = np.zeros(n_points)   # #density
        self.P = np.zeros(n_points) # #pressure
        self.u = np.zeros(n_points) # #velocity
        self.e = np.zeros(n_points) # #internal energy

        for index in range(n_points):
            if self.x[index] < x1:
                #Solution b4 x1
                self.rho[index] = rho_l
                self.P[index] = P_l
                self.u[index] = u_l
            elif x1 <= self.x[index] <= x2:
                #Solution b/w x1 and x2
                c = mu*mu*((x0 - self.x[index])/t) + (1 - mu*mu)*c_l
                self.rho[index] = rho_l*(c/c_l)**(2/(gamma - 1))
                self.P[index] = P_l*(self.rho[index]/rho_l)**(gamma)
                self.u[index] = (1 - mu*mu)*((-(x0-self.x[index])/t) + c_l)
            elif x2 <= self.x[index] <= x3:
                #Solution b/w x2 and x3
                self.rho[index] = rho_middle
                self.P[index] = P_post
                self.u[index] = v_post
            elif x3 <= self.x[index] <= x4:
                #Solution b/w x3 and x4
                self.rho[index] = rho_post
                self.P[index] = P_post
                self.u[index] = v_post
            elif x4 < self.x[index]:
                #Solution after x4
                self.rho[index] = rho_r
                self.P[index] = P_r
                self.u[index] = u_r
            self.e[index] = self.P[index]/((gamma - 1)*self.rho[index])

