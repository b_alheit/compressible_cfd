from modules.compressible_simulation import OneDCompressibleSimulation
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
# markers = ['^', 'X', 's', 'v', 'o', 'x', 'P']
markers = [None, None, None, None, None, None]



def van_albada_fl(phi, phi_L):
    return np.divide(np.power(phi, 2) + np.multiply(phi, phi_L), 1e-20 + np.power(phi, 2) + np.power(phi_L, 2))

def van_leer_fl(phi, phi_L):
    return np.divide(phi + np.multiply(np.sign(phi_L), np.abs(phi)), 1e-20 + phi_L + np.multiply(np.sign(phi_L), np.abs(phi)))

def first_order_fl(phi, phi_L):
    return 0*phi

x_init = np.array([-0.5, 0, 0.5])
rho_init = np.array([1, 0.125])
u_init1 = np.array([np.array([[0]]), np.array([[0]])])   # Case 1
u_init2 = np.array([np.array([[0.25]]), np.array([[0]])])    # Case 2
p_init = np.array([1.0, 0.1])
gamma = 1.4

simulations = np.empty(0, dtype=OneDCompressibleSimulation)
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_albada_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init2, p_init, gamma, van_albada_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_leer_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init2, p_init, gamma, van_leer_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, first_order_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init2, p_init, gamma, first_order_fl)])

labels = ["Case 1 Van Albada", "Case 2 Van Albada", "Case 1 Van Leer", "Case 2 Van Leer", "Case 1 first order", "Case 2 first order"]

for i in range(np.alen(simulations)):
    simulations[i].solve(0.2, 0.002)
    simulations[i].extract_all()

x = simulations[0].x.tolist()

for i in range(np.alen(simulations)):
    hexColour = str(hex(int(i * 260 / (np.alen(simulations)))))[2:]
    if len(hexColour) == 1:
        hexColour = '0' + hexColour

    plt.figure(1)
    plt.plot(x, simulations[i].p.tolist(), color='#' + 3*hexColour, marker=markers[i], label=labels[i])
    # plt.plot(x, simulations[i].p.tolist())
    plt.figure(2)
    plt.plot(x, simulations[i].e.tolist(), color='#' + 3*hexColour, marker=markers[i], label=labels[i])
    plt.figure(3)
    plt.plot(x, simulations[i].rho.tolist(), color='#' + 3*hexColour, marker=markers[i], label=labels[i])
    plt.figure(4)
    plt.plot(x, simulations[i].u.tolist(), color='#' + 3*hexColour, marker=markers[i], label=labels[i])

plt.figure(1)
plt.xlim(-0.5, 0.5)
plt.grid()
plt.xlabel("$x$", fontsize=12)
plt.ylabel("$p$", fontsize=12)
plt.title("Plot of pressure distribution for various simulations", fontsize=14, fontweight='bold')
plt.legend()


plt.figure(2)
plt.xlim(-0.5, 0.5)
plt.grid()
plt.xlabel("$x$", fontsize=12)
plt.ylabel("$e$", fontsize=12)
plt.title("Plot of internal energy distribution for various simulations", fontsize=14, fontweight='bold')
plt.legend()


plt.figure(3)
plt.xlim(-0.5, 0.5)
plt.grid()
plt.xlabel("$x$", fontsize=12)
plt.ylabel("$\\rho$", fontsize=12)
plt.title("Plot of density distribution for various simulations", fontsize=14, fontweight='bold')
plt.legend()


plt.figure(4)
plt.xlim(-0.5, 0.5)
plt.grid()
plt.xlabel("$x$", fontsize=12)
plt.ylabel("$u$", fontsize=12)
plt.title("Plot of velocity distribution for various simulations", fontsize=14, fontweight='bold')
plt.legend()

plt.show()

