import numpy as np

class facet:
    def __init__(self, x, dim):
        """
        vec(u) = [u  v  w]^T
        U = \rho[1  vec(u)  E]^T
        F = U vec(u)^T + p[vec(0)  I  vec(u)^T]^T
        """
        # Dimension of simulation
        self.dim = dim

        # x position of facet
        self.x = x

        # Normal vector of facet
        self.n = np.array([[1]])

        # Initialising some variable
        self.U_L = np.zeros([2+dim, 1])
        self.U_R = np.zeros([2+dim, 1])
        self.dU = np.zeros([2+dim, 1])
        self.KAKU = np.zeros([2+dim, 1])
        self.F_bar = np.zeros([2+dim, 1])

        # Initialising some variable
        self.u_tilde = np.zeros([dim, 1])
        self.u_tilde_sca = None
        self.e_tilde = None
        self.H_tilde = None
        self.a_tilde = None

        self.dx = None

        self.left_node = None
        self.right_node = None

    def set_left_node(self, node):
        # Defining the node left of the face
        self.left_node = node

    def set_right_node(self, node):
        # Defining the node right of the face
        self.right_node = node

    def calc_dx(self):
        # Calculating the distance between the left and the right node
        self.dx = self.right_node.x - self.left_node.x


    def calc_U_LR(self, flux_limiter):
        # Calculating the left and right states of the facet

        dU = self.right_node.U - self.left_node.U

        dU_L = 2 * ((self.right_node.U - self.left_node.left_node.U)/(self.right_node.x - self.left_node.left_node.x)) * self.dx - dU
        dU_R = 2 * ((self.right_node.right_node.U - self.left_node.U)/(self.right_node.right_node.x - self.left_node.x)) * self.dx - dU

        self.U_L = self.left_node.U + 0.5 * np.multiply(flux_limiter(dU, dU_L),  dU_L)
        self.U_R = self.right_node.U - 0.5 * np.multiply(flux_limiter(dU, dU_R), dU_R)

    def calc_tildes(self):
        # Calculating roe averaged values
        self.u_tilde = (self.U_L[0] ** -0.5 * self.U_L[1:-1] + self.U_R[0] ** -0.5 * self.U_R[1:-1])/(self.U_L[0] ** 0.5 + self.U_R[0] ** 0.5)
        self.u_tilde_sca = np.dot(self.u_tilde, self.n)
        self.e_tilde = 0.5 * np.dot(self.u_tilde, self.u_tilde)
        gamma = self.right_node.gamma

        H_L = gamma * self.U_L[-1] / self.U_L[0] + 0.5 * (1-gamma) * np.dot(self.U_L[1:-1], self.U_L[1:-1]) / self.U_L[0]**2
        H_R = gamma * self.U_R[-1] / self.U_R[0] + 0.5 * (1-gamma) * np.dot(self.U_R[1:-1], self.U_R[1:-1]) / self.U_R[0]**2

        self.H_tilde = (self.U_L[0] ** 0.5 * H_L + self.U_R[0] ** 0.5 * H_R)/(self.U_L[0] ** 0.5 + self.U_R[0] ** 0.5)
        self.a_tilde = ((gamma-1) * (self.H_tilde - self.e_tilde))**0.5

    def calc_KAKU(self):
        # Calculating \Delta F_n
        gamma = self.right_node.gamma
        drho = self.U_R[0] - self.U_L[0]
        drho_u = self.U_R[1:-1] - self.U_L[1:-1]
        drho_E = self.U_R[-1] - self.U_L[-1]

        beta1 = ((gamma-1)/(self.a_tilde**2)) * (self.e_tilde * drho - np.dot(self.u_tilde, drho_u) + drho_E)
        beta2 = (np.dot(drho_u, self.n) - self.u_tilde_sca * drho)

        ks = np.array([drho,
                       drho_u,
                       np.dot(self.u_tilde, drho_u) - self.e_tilde * drho])

        ks -= beta1 * np.array([[1],
                                self.u_tilde,
                                self.e_tilde])

        ks -= beta2 * np.array([[0],
                                self.n,
                                self.u_tilde_sca])

        self.KAKU = np.abs(self.u_tilde_sca - self.a_tilde) * ((beta1-beta2)/2) * np.array([[1],
                                                                                            self.u_tilde - self.a_tilde * self.n,
                                                                                            self.H_tilde - self.u_tilde_sca*self.a_tilde])

        self.KAKU += np.abs(self.u_tilde_sca) * ks

        self.KAKU += np.abs(self.u_tilde_sca + self.a_tilde) * ((beta1+beta2)/2) * np.array([[1],
                                                                                            self.u_tilde + self.a_tilde * self.n,
                                                                                            self.H_tilde + self.u_tilde_sca*self.a_tilde])

    def F(self, U):
        # A method to obtain F as a function of U
        U_useful = U.copy()
        U_useful[1:] /= U[0]
        e = U_useful[-1] - 0.5 * np.dot(U_useful[1:-1], U_useful[1:-1])
        p = (self.right_node.gamma - 1) * U[0] * e
        return np.matmul(U, U_useful[1:-1].T) + p * np.concatenate((np.concatenate((np.zeros([1, self.dim]), np.identity(self.dim)), axis=0), U_useful[1:-1].T), axis=0)

    def calc_F_bar(self, flux_limiter):
        # Using the various methods in the class to calculate F_bar
        self.calc_U_LR(flux_limiter)

        self.calc_tildes()

        self.calc_KAKU()

        self.F_bar = 0.5 * (self.F(self.U_L) + self.F(self.U_R)) - 0.5 * self.KAKU



