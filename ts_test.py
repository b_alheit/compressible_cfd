from modules.compressible_simulation import OneDCompressibleSimulation
import numpy as np
import time
from modules.analytical_shock_tube import AnalyticalShockTube
from modules.analytical_shock_tube import AnalyticalShockTube2
from modules import time_stepping_schemes as ts
import matplotlib
import matplotlib.pyplot as plt
# matplotlib.rc('text', usetex=True)
# matplotlib.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
markers = ['^', 'o', 's', 'v', 'X', 'x', 'P']

# Some lists for plotting purposes
# markers = [None, None, None, None, None, None]
styles = ['-', '--', '-.', ':']
names = ["p", "e", "rho", "u"]
ylabels = ["$p$", "$e$", "$\\rho$", "$u$"]
labels = ["Forward Euler", "Heun's method", "RK4", "Second order backward difference"]
solvers = [ts.FE, ts.Heun, ts.RK4]

name = "05"
dt = 5e-3
# name = "025"
# dt = 2.5e-3
# name = "0125"
# dt = 1.25e-3
times = []

# Setting initial conditions and other necessary values
x_init = np.array([-0.5, 0, 0.5])
rho_init = np.array([1, 0.125])
u_init1 = np.array([np.array([[0]]), np.array([[0]])])   # Case 1
p_init = np.array([1.0, 0.1])
gamma = 1.4


# Defining different flux limiters
# def van_albada_fl(phi, phi_L):
#     return np.divide(np.power(phi, 2) + np.multiply(phi, phi_L), 1e-20 + np.power(phi, 2) + np.power(phi_L, 2))


def van_leer_fl(phi, phi_L):
    return np.divide(phi + np.multiply(np.sign(phi_L), np.abs(phi)), 1e-20 + phi_L + np.multiply(np.sign(phi_L), np.abs(phi)))


# def first_order_fl(phi, phi_L):
#     return 0*phi


# Creating an analytical solution object that will have values of the analytical solution stored in it
case1_analytical = AnalyticalShockTube2(1, 0, 1, 0.125, 0, 0.1, 1.4, 0, 0.2, np.linspace(-0.5, 0.5, 1000))

# Adding the results of the analytical solution to matplotlib figures
plt.figure(1)
plt.plot(case1_analytical.x, case1_analytical.P, color='black', linewidth=3, linestyle="-", marker=None, label="Analytical solution")
plt.figure(2)
plt.plot(case1_analytical.x, case1_analytical.e, color='black', linewidth=3, linestyle="-", marker=None, label="Analytical solution")
plt.figure(3)
plt.plot(case1_analytical.x, case1_analytical.rho, color='black', linewidth=3, linestyle="-", marker=None, label="Analytical solution")
plt.figure(4)
plt.plot(case1_analytical.x, case1_analytical.u, color='black', linewidth=3, linestyle="-", marker=None, label="Analytical solution")


# Creating a numpy array of simulation objects
simulations = np.empty(0, dtype=OneDCompressibleSimulation)
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_leer_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_leer_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_leer_fl)])
simulations = np.append(simulations, [OneDCompressibleSimulation(-0.5, 0.5, 100, x_init, rho_init, u_init1, p_init, gamma, van_leer_fl)])

title = "Different time integration methods with a time step of " + str(dt)
# Solving the simulations and extracting the data
for n in range(np.alen(simulations)-1):
    ti = time.time()
    simulations[n].solve(0.2, dt, solvers[n])
    times.append(time.time() - ti)
    simulations[n].extract_all()

ti = time.time()
simulations[-1].solve_SOBD(0.2, dt)
times.append(time.time() - ti)
simulations[-1].extract_all()

# Creating one list to store the x positions since they're the same for each simulation
x = simulations[0].x.tolist()

# Adding each simulation to the afore created figures
for n in range(np.alen(simulations)):
    hexColour = str(hex(int((n+1) * 180 / (np.alen(simulations)))))[2:]
    if len(hexColour) == 1:
        hexColour = '0' + hexColour
    colour = '#' + 3 * hexColour
    n_markers = 25
    markers_on = range(int(n*n_markers/np.alen(simulations)), int(np.alen(simulations[n].x)-1), int((np.alen(simulations[n].x)-1)/n_markers))
    plt.figure(1)
    plt.plot(x, simulations[n].p.tolist(), color=colour, markerfacecolor="None", markeredgecolor=colour, linewidth=2, linestyle=styles[n], marker=markers[n], label=labels[n], markevery=markers_on)
    plt.figure(2)
    plt.plot(x, simulations[n].e.tolist(), color=colour, markerfacecolor="None", markeredgecolor=colour, linewidth=2, linestyle=styles[n], marker=markers[n], label=labels[n], markevery=markers_on)
    plt.figure(3)
    plt.plot(x, simulations[n].rho.tolist(), color=colour, markerfacecolor="None", markeredgecolor=colour, linewidth=2, linestyle=styles[n], marker=markers[n], label=labels[n], markevery=markers_on)
    plt.figure(4)
    plt.plot(x, simulations[n].u.tolist(), color=colour, markerfacecolor="None", markeredgecolor=colour, linewidth=2, linestyle=styles[n], marker=markers[n], label=labels[n], markevery=markers_on)

for i in range(np.alen(simulations)):
    print(labels[i], " solve time: ", times[i]," s")

# Formatting the figures
for i in range(4):
    n = i+1
    plt.figure(n)
    plt.xlim(-0.5, 0.5)
    plt.grid()
    plt.xlabel("$x$", fontsize=12)
    plt.ylabel(ylabels[i], fontsize=12)
    plt.legend()
    # plt.title(title)
    plt.figure(n).set_figwidth(10.24)
    plt.figure(n).set_figheight(3.2)
    plt.figure(n).tight_layout()
    # plt.savefig("./assignment2_document/figures/" + names[i] + name + ".png")

plt.show()

